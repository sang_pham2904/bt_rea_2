import React, { Component } from "react";
import { glassArr } from "./dataGlass.js";

export default class ShopGlass extends Component {
  log = () => {
    console.log(glassArr);
  };
  state = {
    glass: glassArr[0],
  };
  renderGlass = () => {
    return glassArr.map((item) => {
      return (
        <a classname="" onClick={() => this.handleChangeGlass(item)}>
          <img
            style={{ width: "200px" }}
            className="px-3"
            src={item.link}
            alt
          />
        </a>
      );
    });
  };
  handleChangeGlass = (idGlass) => {
    this.setState({
      glass: idGlass,
    });
  };
  render() {
    return (
      <div className="">
        <div className="header">
          <h3 className="text-white py-2">TRY GLASES APP ONLINE</h3>
        </div>
        <div className="body pt-5 container">
          <div className="picture row">
            <div className="col-6">
              <div className="tester">
                <img
                  style={{ width: " 200px" }}
                  className=""
                  src="./img/model.jpg"
                  alt=""
                />
                <div className="glassToTest">
                  <img
                    className="glassTest"
                    src={this.state.glass.url}
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div className="col-6">
              <img style={{ width: " 200px" }} src="./img/model.jpg" alt="" />
            </div>
          </div>
          <div className=" mt-5 glass bg-white">{this.renderGlass()}</div>
        </div>
      </div>
    );
  }
}
